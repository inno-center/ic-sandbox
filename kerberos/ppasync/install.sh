#!/bin/sh
set -e -x
echo "===== Installation script of ppasync ====="
echo "===== STEP 0 install git & python3 ====="
echo ""
yum -y install git
yum -y install python36-devel
yum -y install python36-setuptools
easy_install-3.6 pip
echo "[OK]"
echo "===== STEP 1 install Development Tools ====="
echo ""
yum -y groupinstall "Development tools"
echo "[OK]"
echo "===== STEP 2 compile librdkafka from source ====="
echo ""
rm -rf /tmp/librdkafka && git clone https://github.com/edenhill/librdkafka /tmp/librdkafka
(cd /tmp/librdkafka && ./configure --enable-sasl --prefix=/tmp/librdkafka/INSTALL && make clean all && make && make install)
echo "[OK]"
echo "===== STEP 3 compile confluent_kafka package ====="
echo ""
rm -rf /tmp/confluent-kafka-python && git clone https://github.com/confluentinc/confluent-kafka-python /tmp/confluent-kafka-python
(cd /tmp/confluent-kafka-python && C_INCLUDE_PATH=/tmp/librdkafka/INSTALL/include LIBRARY_PATH=/tmp/librdkafka/INSTALL/lib python3.6 setup.py build && C_INCLUDE_PATH=/tmp/librdkafka/INSTALL/include LIBRARY_PATH=/tmp/librdkafka/INSTALL/lib python3.6 setup.py install)
echo "[OK]"
echo "===== STEP 4 install pip packages ====="
echo ""
pip3.6 install click
pip3.6 install simplejson
pip3.6 install lxml
pip3.6 install regex
pip3.6 install requests
echo "[OK]"
echo "===== STEP 5 put keytab certificate ====="
echo ""
touch "/tmp/uki.keytab"
echo "[SKIP]"
echo "===== STEP 6 update cron ====="
echo ""
cat /etc/crontab
echo "[SKIP]"
echo "BYE"
